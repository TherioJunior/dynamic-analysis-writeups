# Dynamic Analysis Writeups

# Introduction

This is a repository where I do [dynamic analyses](https://totalview.io/blog/what-dynamic-analysis) of applications I have an interest in or popular applications in general. This repository may or may not die or get discontinued in the future, my initial plan for now was simply to analyse [Opera GX](https://www.opera.com/gx) because of its popularity.

I am very invested in digital privacy, and Opera Limited, while being a Norwegian company, is owned by Beijing Kunlun Tech Co., Ltd., and Keeneyes Future Holdings Inc, and Chinese companies aren't the greatest when it comes to digital privacy, so I wanted to look into it :).

Source: [Wikipedia](https://en.wikipedia.org/wiki/Opera_(company)).

# Writeups

If you want to head straight into the writeups, please refer to [this page](./Writeups/README.md). If you want to read about my plan or what I use for these writeups, read further below.

# The plan

I plan to do organized writeups with proof of what I found in this repository. I'll try to have them well structured, differentiating between the first start right after installing an application, and continous usage when using the application. More about how I do this below.

# My setup and what I use for these writeups

### The platform

I use Linux as my primary OS, with the [QEMU/KVM](https://www.qemu.org/) Hypervisor installed. This is managed through [libvirt](https://libvirt.org) and the [Virtual Machine Manager](https://virt-manager.org/) application.

The Virtual Machine I am doing these analyses on, is a `Windows 11` Virtual Machine, only slightly changing the defaults, like disabling location services, uninstalling OneDrive, etc.

### The tools

On this Virtual Machine I have a variety of tools installed, most of which I may or may not use. These include:

- [PEStudio](https://www.winitor.com/) prior to running an application to analyse its behaviour inside a sandbox.

- [WireShark](https://www.wireshark.org/) and [Fiddler](https://www.telerik.com/fiddler) to analyse network packets sent and received. I decrypt HTTPS for this via Fiddler's inbuilt features, to be able to see what is sent to- and back from the server.

- [x64dbg](https://x64dbg.com/) and [API Monitor](https://www.rohitab.com/apimonitor) to analyse, amongst other things, Filesystem and Windows Registry calls.

- The entirety of the [Sysinternals suite](https://learn.microsoft.com/en-us/sysinternals/downloads/sysinternals-suite), though this is used less often.
