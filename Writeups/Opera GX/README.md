# Opera GX Writeup - Dynamic Analysis of the popular gaming browser

This is where I will be doing a writeup on what Opera GX does when you're running it, potential telemetry, what gets sent where, location services, etc. Everything that's related to what the program does, for now mostly web-request wise, will be written down here.

# Bookmarks

The initial startup is quite detailed, whereas the continous usage will more so focus on "shady" practices and more than questionable privacy things, so I'll include bookmarks to the different sections here:

- [The Conclusion](#conclusion)

- [The First startup after the installation](#the-first-startup-after-the-installation)

- [Continous usage](#continous-usage)

# Conclusion

- First startup privacy: `Good, nothing seems to get stolen or extracted`.

- First startup security practices: `Good I'd assume, uses HTTPS for most of its requests, only some are HTTP which are unimportant from what it looks like`.

- Continous usage privacy: `Not terrible, but not great either. More below`.
  
  - Incorporates tracking, ads (not personalized) and marketing campaigns by default (opt-out)
  
  - AI Assistant provided, in part, by Google.
  
  - Builtin VPN (opt-in), where you can't know what they're doing with your data.
  
  - Unique identifiers to your browser and PC being used for tracking on various domains.
  
  - Has third party ad and marketing partners such as: Google Analytics, Bing, Facebook Pixel and others.

- Continous usage security practices: `Good`.

My conclusion of this browser would be to stay away from it if you care about your digital privacy and safety of your data, and stick to safer, more privacy focused and open-source solutions such as Firefox. If you don't like non-chromium browsers, Brave would be a viable alternative.

If there is interest, I can continue doing this analysis and add on top of it, try to make it more privacy focused and maybe provide a guide for it, as well as see what happens when using a signed in Opera account. If you're interested in that, feel free to message me via a way of your choice by referring to the [description of my GitLab account](https://gitlab.com/TherioJunior).

# The first startup after the installation

The first two requests that are being made are sent to the host `sitecheck.opera.com/api/v2/check`. I cannot directly deciper what's being sent and received here, but it comes down to the following non critical stuff:

- Browser sends a POST request to aforementioned site with `Content-Type: application/x-protobuf`

- Response from the server are the domains `gxcorner.games` and `redir.opera.com` for requests one and two respectively.

After that, a GET response to `autoupdate.geo.opera.com/geolocation/` is sent. The response is a JSON answer in the following format:

```json
{
    "country": "<two letter country code of your origin here",
    "timestamp": "<UNIX timestamp, type integer>"
}
```

Then a few CONNECT tunnels to `sd-images.operacdn.com` are opened, and after successful connection a few GET requests are sent, presumably to get some pictures of whatever.

Then a GET request is sent to `proxy.gxcorner.games` with previously collected country data, to get the daily wallpapers from what I'd assume, based on the header information.

</details>

Then a GET request is sent to `redir.opera.com/www.opera.com/gx/firstrun/` which redirects you to `https://www.opera.com/client/welcome-gx?`.

Then more image downloads

Then a POST request to `sitecheck.opera.com/api/v2/check`, of which the response is `www.opera.com`.

Then more image downloads

Because this is the first run, we then get two opened tabs with URLs `gxcorner.games` and `www.opera.com/client/welcome-gx?` which are just the starting pages after you're opening the browser for the first time.

Then 3 GET requests are made to `exchange.opera.com/api/v1/cmc`, `exchange.opera.com/api/v1/ecb` and `exchange.opera.com/api/v1/nbu` respectively, which are used for currency prices, both based on the requested URL and based on the JSON response.

Then a GET request to `features.opera-api2.com/api/v2/features?<glibberish>` is sent, which judging by the JSON response is used to enable and disable certain features of the browser, based on your country and presumably its privacy rights, common practice I think. Request headers include your country, system language, a presumably (have no proof of this) unique UUID, product which is GX in this case, your release channel which is `Stable` in this case and the browser version.

Then a GET request is sent to `proxy.gxcorner.games` to a `/new-content/` location, which judging by the JSON response is used to get the latest titles and thumbnail URLs for the GX Corner.

Then 2 GET requests to `extension-updates.opera.com` with some information about your system architecture and release channel, maybe to check for updates of the browser or inbuilt extensions.

Then a GET request to the same domain as above is made, which results in a `404` response, meaning the requested content couldn't be found.

Then a GET request to `gxcorner.games/assets/index-c469f4f5.js` is sent, whos response is minified JavaScript code, which contains some locale information and page URLs.

Then 4 GET requests are made to the same domain as above, 1 of which loads minified CSS whereas the others load more minified JavaScript code.

Then 2 GET requests are made to `extension-updates.opera.com`, one of which checks the same thing the first request to this URL did, while the other checks for some other App IDs, which are presumably just inbuilt extensions which are checked for updates on startup.

Then a GET request to `weather.opera-api2.com/location?id=254946` and your language is made, which is just the default Opera GX location code of Oslo (very nice there btw), where the response is obviously just the current weather in Oslo in JSON format.

Then 2 GET requests are made to `cdn-production-opera-website.operacdn.com` are made, one of which is related to the GX welcome screen, while the other is related to some embeddedVideo from what I can judge.

Then some fonts.googleapis.com sites are hit up via GET requests.

Then a GET request to `gxcorner.games/assets/App-5843cb2e.css` is made, which seems to just be CSS for some inbuilt icons and inbuilt page design.

Bunch of more HTTP GET requests are made to `cdn-production-opera-website.operacdn.com` and `proxy.gxcorner.games` which seem to load JavaScript code and CSS stylesheets.

Then a POST request to `sentry-relay.opera-api.com` is made with a, presumably, uniquely generated sentry_key parameter, the sentry_version (7 in this case) and the sentry_client which is sentry.javascript.svelte. The response of this request is a empty JSON array.

Then 10 GET requests are made to `gxcorner.games`, 6 of which load minified JavaScript code, 3 of which are minified CSS and 1 `favicon.png`, which are more than likely, again, just for inbuilt page design.

Then a GET request to `proxy.gxcorner.games/new-content/corners/desktop/<your-country-2-letter-name>/<your-system-language-2-letter-name>` is sent, the response of which is a relatively long JSON document, which is presumably just information that's being displayed inside the GX Corner, such as upcoming games, sales, free-to-play state and other stuff.

Then some `fonts.gstatic.com` requests, a lot more minified JavaScript and CSS from `proxy.gxcorner.games` and `gxcorner.games`, some "popup badges" as they call it, some free to play data and generally uninteresting stuff.

Then a GET request to `www.opera.com/api/geolocation/` whos response is in JSON, which is structured as follows in my case as the VPN server I am using is in germany:

```json
{
    "buckets": [
        "vpn-pro-eurozone",
        "eea",
        "euro_russia_namerica"
    ],
    "continent": "EU",
    "country": "DE",
    "ip": "<your IPV4 address here>"
}
```

Then a GET request to `proxy.gxcorner.games/cloud-gaming-services` with parameters serviceKey=blacknut and your system/browser language, which returns a very long JSON response, listing various games, alphabetically sorted with their redirect link to `blacknut.com`.

Then more CSS and minified JavaScript, as well as requests to `addons.opera.com` which point to theme locations, another `404` (Not Found) request to `extension-updates.opera.com`.

Then multiple POST requests to `android.clients.google.com/c2dm/register3` which seem to register some unique token.

Then some more `addons-extensions.operacdn.com` requests to presumably download media.

Then a request to `gxcorner.games/cookie-script/cookie-script.js` which is relatively self explanatory based on the name I'd hope.

Then some requests to `i.ytimg.com` downloading JPG files, followed by `gxcorner.games/cookie-script/sdk_cmp.js` who's referrer is another minified JavaScript we loaded some time ago, also from `gxcorner.games`, which we can tell by looking at the referrer.

A lot of sending webp files to `proxy.gxcorner.games/resizer/`.

Then requests to `addons.opera.com` to a `/extensions/api/content-verification` location, presumably to verify extension hashes to make sure nothing was corrupted during the installation.

Then we have 5 GET requests to `download3.operacdn.com` which download default search engine presets in JSON format, containing different search engine URLs for the different country-codes. This is not submitted to the server, instead presumably handled locally. Another one gets preferences and another one gets keywords for I don't know what. One other request to that location is a .gz file named "hint", the content of which I cannot view inside Fiddler and the last one is a partnerrules JSON file, containing the following fields:

```json
{
    "banned_partner_content_search_engine_partner_ids": [],
    "checkout_rules": [],
    "page_view_rules": [],
    "partner_id_rules": [],
    "search_rules": []
}
```

Then a request to `update.googleapis.com` checking the latest chromium version from what I assume, hard to judge, followed by a LOT of `edgedl.me.gvt1.com` requests, which presumably download chromium `.crx3` files, judging by the request.

All these events happened in around 1 minute of letting the browser idle after starting it for the very first time. Writing this took a lot longer whoops. Amazing how fast technology is nowadays.

# Continous usage

So to start of with this, the first requests made are largely similar to those done on the first startup. We'll now delve deeper, look at what the browser does when you're browsing the web, saving bookmarks, interacting with it, etc.

I didn't sign into their browser, so the list of features available to me was limited. For example, I was not able to use "Aria", their AI powered browser assistant, but this is apparently based on a combination of OpenAI and Google, so make of that what you will, especially regarding Google.

After some continous usage of Opera GX over not even a lot of time, you'll start to notice various analytic and tracking related requests to various domain. Some of those belong to Opera themselves, others to Google.

If you'd use the browser further, you'd probably see a lot of other tracking, ad and analytic data being sent and received, since judging by their [Privacy Policy](https://legal.opera.com/privacy/), [Cookie Policy](https://legal.opera.com/cookies/) and [EULA](https://legal.opera.com/eula/computers/), while they claim to align with GDPR guidelines, have third party partners including Google Analytics, Bing, Facebook Pixel and others, with the primary usage of all this being third-party cookies for analytics and marketing campaigns.

Additionally, while they are mostly in line with GDPR guidelines from what I can judge based on their behaviour, all of the really privacy related stuff, such as `Improve search suggestions`, `Display promotional notifications`, `Receive promotional SPeed Dials, bookmarks and campaigns`, which are based on your usage and information you give them, are all opt-out.

But to their credit, `Personalized content`, `Ads personalized based on your interests`, `General location` and `General interests based on web sites you may visit or search` are all disabled by default.

Judging by network sniffing, you still have to tweak the other settings though, as marketing related cookie consent is enabled by default, as seen below:

```json
Cookie: cookie-consent={%22marketing%22:true%2C%22necessary%22:true%2C%22version%22:2}; 
```

Additionally, your browser or your PC get a unique identifier for said marketing, tracking and ad campaigns.

To add on top of this, while their VPN, AI Assistant and Opera Account are opt-in, you don't know what happens with that data at that point, especially if you're using their VPN and AI Assistant (which is provided, in part, by Google, judging from their first-use popup about privacy and consent).

The conclusion of the contious usage here would be that while you may be able to use it semi-privacy focused if you adjust certain standard setting, and don't use various features built into the browser, it is probably not recommended if you're remotely privacy concious.
